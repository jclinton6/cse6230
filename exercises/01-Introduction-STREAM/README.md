# Exercise 1: Introduction & STREAM

* Due: Friday, August 25, 2017 [Anywhere on Earth](https://www.timeanddate.com/time/zones/aoe)

* Please submit your exercise to T-Square.

---

* 0) Please let me know if there is a topic that you are interested in for the
  final project.  A link to a relevant research paper would be awesome!

Please answer the questions below about the computer system where you are
completing this assignment.  For all questions, please indicate how (system
commands, external documentation) you arrived at your answer.

* 1) What is the name/type of your CPU microarchitecture?

* 2) What is the frequency of your processor?

* 3) How many levels of data cache does your system have, and how big are they?

* 4) What type of system memory do you have?  What is its maximum frequency (in
  terms of bytes/second)?

* 5) What is the peak theoretical bandwidth for each level of the memory
  hierarchy?

* 6) [Download] the STREAM benchmark.  Use the benchmark with the following
  Makefile script (or the Makefile in [the notes](../../notes/STREAM/Makefile)):

  [Download]: https://www.cs.virginia.edu/stream/FTP/Code/stream.c

```make
CC = cc
CFLAGS = -O0
N = 2000000

.PHONY: runstream

runstream:
	$(CC) $(CFLAGS) -DSTREAM_ARRAY_SIZE=$(N) -o stream stream.c
	./stream
```

  So, for example, run `make runstream N=3000000`.  You may change the compiler
  to one that works on your system.

* 7) Create a plot that shows the output of `make runstream` for each of the
  subbenchmarks (`Copy`, `Scale`, `Add`, `Triad`).

  (Because the benchmarks have different numbers of load/stores, you should
  choose `Words` as your x-axis. For example, `Copy` loads 1 and stores 1 word
  per loop, so `Words` = 2 * `N`.)

  Choose enough values of `N` that you think demonstrate the memory bandwidth
  from each level of the memory hierarchy.  Label on the plot the bandwidths
  you computed in (5).  Do your best to choose `CFLAGS` that maximize the
  reported bandwidth, and report your choice.

* 8) Using your optimized `CFLAGS`, can you find a large value of `N` (so that
  no vector fits in cache) that performs significantly *worse* than nearby
  values?  (Hint: why does `stream.c` have an `OFFSET` preprocessor variable?)

