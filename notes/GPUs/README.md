# GPUs

## Introduction to GPUs and CUDA

We will start with Professor Vuduc's [notes](http://vuduc.org/cse6230/slides/cse6230-fa14--05-cuda.pdf) from 2014.

There are some updates to make since that time:

---

- We can get updates on the comparison of CPU and GPU (and Manycore) performance as recent as 2016 from [Karl Rupp's website](https://www.karlrupp.net/2013/06/cpu-gpu-and-mic-hardware-characteristics-over-time/).  Here is an overview:
    - A modern GPU/Manycore device (NVIDIA Tesla P100 Pascal, Intel Xeon Phi KNL) dissipates more heat / uses more power than a dual-socket CPU node (Intel Xeon CPU) at about a rate of 5:3.
    - GPU/Manycore device has about 7x FLOP/sec available (most
      of those flops on a CPU/Manycore come from vector units (SIMD) as opposed
      to the streaming multiprocessors (SIMT)).
    - Thus, GPU/Manycore are about 3x-4x more efficient in terms of FLOP/Watt.
    - The number of hardware threads per streaming multiprocessor (SM) has been decreasing with successive NVIDIA architectures, but the number of SMs has increased.
    - Device memory bandwidth is about 80 GB/s for and Intel CPU dual socket, 500 GB/s for Intel Xeon Phi KNL, and 600 GB/s NVIDIA NVIDIA Pascal.
    - *The system balance of CPUs and GPUs are actually quite similar now*, with Intel Xeon CPUs actually having more FLOPs/byte of double precision than NVIDIA P100.

- Perhaps just as consequential as the increases in device memory bandwidth
  since Prof. Vuduc's slides is the improvement in host-device transfer rates,
  particularly from [NVLink](https://en.wikipedia.org/wiki/NVLink).  Advances
  in PCI-e took 6GB/s of bandwidth to 32 GB/s, but NVLink 1.0 has 160 GB/s of
  streaming transfer rate and NVLink 2.0 has (will have?) 300 GB/s of bandwidth.  

- The advent of the [unified memory model in
  CUDA](https://devblogs.nvidia.com/parallelforall/unified-memory-in-cuda-6/)
  means that explicit memory transfers between host and device are no longer
  *necessary*.  However, they will still achieve better performance.  Explicit
  [prefetching](http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html)
  is a way to use the unified memory model while still specifying when memory
  is available on the CPU or GPU.  Note that NVIDIA's is essentially building
  its own cache and memory management into the runtime, and thus facing a lot
  of the same issues that occur on the multicore sockets we've seen already,
  like cache-coherence.  In general, memory is not coherent until
  synchronization.

- The syntax of kernel launching now optionally includes explicit
  [stream](http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#streams)
  selection, like `<<< num_blocks, threads_per_block, prealloc_shared_mem,
  stream_id >>>`.

- The memory hierarchy within a P100 (according to the [white paper](https://images.nvidia.com/content/pdf/tesla/whitepaper/pascal-architecture-whitepaper.pdf)) has a 64KB pool of shared memory per SM and a separate L1/texture cache.  There is a unified 4MB L2 cache for the whole device.



## Additional Reading

- Prof. Vuduc's [notes](http://vuduc.org/cse6230/slides/cse6230-fa14--05-cuda.pdf)
- Prof. Vuduc's [tuning notes](http://vuduc.org/cse6230/slides/cse6230-fa14--07-gpu-tuning-1.pdf)
- Karl Rupps's [performance notes](https://www.karlrupp.net/2013/06/cpu-gpu-and-mic-hardware-characteristics-over-time/)
- NVIDIA's [CUDA documentation](http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html)
- NVIDIA's [Performance Profiling Slides](http://on-demand.gputechconf.com/gtc/2016/presentation/s6810-swapna-matwankar-optimizing-application-performance-cuda-tools.pdf)
