
# System Balance

## Why does system balance matter?

* When we think about HPC applications, we often think about problems that are "large":
  large enough that the asymptotic analysis for time and space complexity become relevant in practice. 

  (This is a statement that centers "weak-scaling", and it is not true of all
  HPC applications.  Pushing the time to solution of a fixed problem size --
  "strong-scaling" -- is increasingly important, for example to climate
  simulation.  For further discussion see [slides 26 & 27 from Jed Brown's
  talk](https://jedbrown.org/files/20160217-CISLVersatility.pdf).  For now
  let's accept the weak-scaling conceit.) 

* Because of the relevance of asymptotic analysis, we are almost always bound
  to "fast" algorithms: algorithms that are within a polylog factor of linear
  complexity.

---

* Example (due to Ulrich Rüde): Solving a linear system with a trillion unknowns
  is possible on current supercomputers.  A dense matrix vector multiplication
  for a vector with a trillion unknowns (at least a yottaFLOP), using the power
  profile of Sunway TaihuLight (93 petaFLOPS / 15 megawatts = 6.2 billion flops
  per Joule) would require ~1.6 million gigajoules (~0.3 megatons) of energy.

---

* So, much effort goes into optimizing algorithms with a more-or-less fixed
  ratio of floating point operations (FLOPs) to memory operations (MEMOPs).
  This ratio (FLOPs / MEMOPs) for an algorithm (or easily analyzed subset of an
  algorithm, often called a *kernel*), is called its *arithmetic intensity*.  

* The same ratio, when referring to the raw capabilities of a computer, is a type of
  **System Balance**.

* What determines that ratio: is it the application, or is it the hardware?

    * The algorithm is designed to solve the application, but it must run on the
      hardware, so each has a say.

    * Often there are design parameters: a spectrum of (asymptotically) efficient
      with varying degrees of arithmetic intensity.

---

* ["Memory Bandwidth and Sysem Balance in HPC Systems" slides from John McCalpin, starting on 'What are "System Balances?"' slide][McCalpin].

* The trend is relentlessly diminishing GWords/GFLOP.  Two results:

    * Between two competing algorithms that solve the same problem, the one with
      *higher* arithmetic intensity has a better chance of achieving high performance.

    * Memory bandwidth is the limiting factor for many kernels, so knowing the
      *peak sustainable* memory bandwidth of a system is critical.

        * A benchmark appears!

## STREAM

* *A Survey of Memory Bandwidth and Machine Balance in Current High Performance Computers*,
  [McCalpin, 1995][streampaper]

~~~~
for j=1:NTIMES
  // time each of these vector operations separately
  C[:] = A[:]            // Copy
  B[:] = s * C[:]        // Scale
  C[:] = A[:] + B[:]     // Add
  A[:] = B[:] + s * C[:] // Triad
  // aggregate statistics (min, max, avg)
~~~~

* See the `Makefile` in this directory to get and run the STREAM benchmark

* See [exercise 01](../../exercises/01-Introduction-STREAM/)

## Notes on Exercise 01

Resources for finding out which processor you have and what its frequency is:

* `cat /proc/cpuinfo`
* `lshw`

---

Resources for finding out how big your caches are and what kind of main memory you have:

* `sudo lshw`
* [ark.intel.com](https://ark.intel.com)
* [developer.amd.com](http://developer.amd.com/resources/developer-guides-manuals/)
* [en.wikchip.org](https://en.wikichip.org/wiki/WikiChip)

---

Resources for finding out how much peak theoretical main memory bandwidth you have:

* See above
* Memory types are usually named by its *Megatransactions per second per channel*, MT/s, e.g. DDR3-1866
    * 1 Transaction = 1 word = 64 bits = 8 bytes
    * How many channels is determined by the processor (again, see above Intel/AMD/wikichip resources)

---
    
Resources for finding out how much peak theoretical bandwidth for your memory hierarchy (L1/L2/L3):

* This one is tricky, even according to "Dr. Bandwidth" John McCalpin: [www](https://software.intel.com/en-us/forums/software-tuning-performance-optimization-platform-monitoring/topic/532346)
* Don't worry too much about this part: cache operations make simple "peak performance" bandwidths hard to formulate
* You might see a value of the form "32B/cycle `LX`->`LY`" or similar: use this and the clock speed to get an upper bound on the bandwidth from `LX`

---

The research paper that I showed that demonstrated the bandwidth diagrams is here: [www](https://www.researchgate.net/publication/221552496_Analyzing_Cache_Bandwidth_on_the_Intel_Core_2_Architecture).

## Additional Reading

* ["Memory Bandwidth and Sysem Balance in HPC Systems (McCalpin)"][McCalpin]
* ["A Survey of Memory Bandwidth and Machine Balance in Current High Performance Computers (McCalpin, 1995)"][streampaper]

[McCalpin]: https://sites.utexas.edu/jdm4372/2016/11/22/sc16-invited-talk-memory-bandwidth-and-system-balance-in-hpc-systems/ "Memory Bandwidth and Sysem Balance in HPC Systems (McCalpin)"
[streampaper]: https://www.cs.virginia.edu/~mccalpin/papers/balance/ "A Survey of Memory Bandwidth and Machine Balance in Current High Performance Computers (McCalpin, 1995)"

