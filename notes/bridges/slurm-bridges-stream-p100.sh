#!/bin/sh
#SBATCH  -J stream-p100                  # Job name
#SBATCH  -p GPU-shared                   # Queue (RM, RM-shared, GPU, GPU-shared)
#SBATCH  -N 1                            # Number of nodes
#SBATCH --gres=gpu:p100:1                # GPU type and amount
#SBATCH  -t 00:05:00                     # Time limit hrs:min:sec
#SBATCH  -o stream-p100-%j.out           # Standard output and error log

git rev-parse HEAD

git diff-files

pwd; hostname; date

./streamcu

date
