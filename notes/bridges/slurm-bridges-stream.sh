#!/bin/sh
#SBATCH  -J stream                       # Job name
#SBATCH  -p GPU                          # Queue (RM, RM-shared, GPU, GPU-shared)
#SBATCH  -N 1                            # Number of nodes
#SBATCH --gres=gpu:k80:4                 # GPU type and amount
#SBATCH  -t 00:05:00                     # Time limit hrs:min:sec
#SBATCH  -o stream-%j.out                # Standard output and error log

git rev-parse HEAD

git diff-files

pwd; hostname; date

./stream

date
