# HPCToolkit on Stampede2

- Make sure you compile your executable with debugging flags `-g`
- `module load hpctoolkit`
- Insert `hpcrun -t` into your command like `ibrun tacc_affinity hpcrun -t ./myprog` to collect measurements
- When your jobscript completes, there should be a directory like `hpctoolkit-jobname-measurements-jobnumber`
- Analyze your executable: `hpcstruct myprog` will create `myprog.hpcstruct`
- Create a profiling databases: `hpcprof -S myprog.hpcstruct hpctoolkit-jobname-measurements-jobnumber/`
- This will create a `hpctoolkit-jobname-database-jobnumber` directory
- Tar it up: `tar -czf hpctoolkit-jobname-database-jobnumber.tgz hpctoolkit-jobname-database-jobnumber/`
- Copy it to your laptop/workstation `scp userid@stampede2.tacc.utexas.edu:/path/to/hpctoolkit-jobname-database-jobnumber.tgz hpctoolkit-jobname-database-jobnumber.tgz`
- Untar it locally: `tar -xzf hpctoolkit-jobname-database-jobnumber.tgz`
- Get the `hpctraceviewer` program from [http://hpctoolkit.org/download/hpcviewer/](http://hpctoolkit.org/download/hpcviewer/)
- [Optional] put it in your shell `PATH`
- `hpctraceviewer hpctoolkit-jobname-database-jobnumber &`
- Read the [documentation](http://hpctoolkit.org/man/hpctraceviewer.html) about how to use it.
- By default, your trace view will include traces for your program's main thread and MPI thread.  You can filter out all your MPI threads by filtering out threads with id `1`.
