# Project 1: Molecular Dynamics

**Due Monday, September 18**

**Resubmission for script-based correctness & performance points due ~~Monday, October 30~~ Monday, November 6**

- The points resulting from script-based scoring of correctness & performance will be averaged over the original submission and the resubmission.

**Your repo transfer request** (name format `cse6230fa17-proj1-gtusername` must be submitted by **11:59:99 p.m. on that day**

## Rules

- Write a shared memory parallel code for Brownian dynamics simulation (with Gaussian random variables as currently demonstrated in `bd.c`) with steric (the type of forces in exercise 04) interactions.  All constants should be as currently defined in the files in this directory.  If the problem is under-defined, I will update as soon as I am made aware.
    * Particle radius `a=1`
    * Time step `dt=1.e-4`
    * Mass `M=1.` (can be left out)
    * Repulsive coefficient `krepul=100.`
    * Periodic domain width `L` is problem dependent. Periodicity should be handled as it is currently handled in `interactions.c`:
        * For the purposes of computing interactions, the distance between particles is defined by being equivalent to `remainder(pi - pj,L)` for each component
        * For the purposes of computing an accurate diffusion coefficient, *periodicity is not enforced on the position itself*
- Results should be output in the *xyz* file format from Prof. Chow's lecture 7
    * Every 1 interval = 1000 time steps, write a *frame* to the output file
    * The number of intervals is problem dependent
    * The starting positions are problem dependent
- Main effort will be to parallelize the interactions function and to compute the repulsive forces in parallel
- Instructors will evaluate performance:
    * Checking out your repo on Deepthought
    * In a batch job, our script will:
        * `cd` to the top directory of your repo (`master` branch)
        * run `make INFILE=aaa.xyz OUTFILE=bbb.xyz NINTERVALS=N CSE6230UTILSDIR=/dir/dir/dir`
        * time this call
        * run the output trajectories `bbb.xyz` through a script like
          `a_msd3.m` in this directory to compute the diffusion coefficient of
          your results

## Grading

- 0-3 points for hassle-free usage: maximized if your code runs the first time from the `make` command in our script
    * Points lost if we have to figure out how to get the output we want
- 0-6 points for a parallel implementation that produces the correct results
    * Correctness is checked by computing the diffusion coefficient
        * **For the example input file included with the code (`lac1_nov12xyz`), the diffusion coefficient should be about 0.7 -- 0.9**
    * Points will be deducted for incomprehensible code: optimized code is complicated, but explainable (with comments!)
        * Code is an intellectual product
- 0-6 Points for speed:
    * fastest code guaranteed 6
    * 0 points for as bad or worse than what I can achieve without parallelizing `interactions`
- 0-3 points for report (`proj1.pdf`, checked into repository)
    * graph of time per timestep for 10000 particles at 20 percent volume
        * **You do not need to run these tests for very long: please use `NINTERVALS=20` for your test**
      fraction (use logarithmic y-axis) vs. number of threads
        * plausible reproducibility: save the jobscripts that generated the
          figure, report the *version of your code (git commit)* used
    * describe your final parallelization and why you consider your parallelization to be faster than other choices

## Advice

- **Understand your code before you try to change it:**
    - You could do this by instrumenting your own code (putting separate timers
      for all of the individual sections of code to see where time is being
      spent)
    - Or you could do this using a performance profilers.  One that is
      available on Deepthought is `gprof`.  You can look at the manual for a
      lot of info on how to use it (`man gprof`), but here is a basic usage on
      Deepthought:
        - Compile your code with the `-pg` flag.  You can do this using the
          Makefile I provided with `make CC="gcc -pg"`.
        - After you run your program (`harness` in our case), a file named
          `gmon.out` will be generated with profiling info
        - Run `gprof` with the name of the program and pipe the results to a
          pager, e.g. `gprof ./harness | less`.  This will give you statistics
          about how much time is being spent in each function.
        - You might want to split your code up into more functions to get a
          better idea about what time is being spent where.
- **Avoid memory contention:**
    - Anytime multiple threads are trying to write to locations close to each
      other, it makes it difficult and expensive to make sure each thread has
      an up-to-date copy of the memory that is changing.  This would happen,
      for example, if many threads are writing to the `pairs` list in
      `interaction()` function.  Consider allocating a separate workspace for
      each thread by, for example, giving each thread its own `pairs` array.
      Then, once all threads are done computing their pairs, you can combine
      the separate arrays into one array, or even change the interface of the
      `interactions()` function so that it is multiple lists are returned.
- **You get to choose how many threads we use to evaluate your code:**
    - There's nothing inherently wrong with achieving your best performance
      using fewer than the maximum number of threads available on a node.  The
      problem may simply not have enough concurrency to support every thread.
      Because you get to set the `HARNESS_ENV` in the Makefile, you can choose
      the best number of threads for your code.
- **Using the `cse6230nrand()` random number generator:**
    - Look at my implementation of [exercise 03](https://bitbucket.org/cse6230fa17/cse6230/src/master/exercises/03-Brownian-Motion/ex03.c)
      which shows how to use the similar `cse6230rand()` function:
        - Have a separate `cse6230nrand_t` object for each thread
        - Initialize that object with a different key for each thread like `cse6230nrand_seed(thread_seed,&nrand)`
        - Generate numbers with `cse6230nrand(&nrand)`
- **Baseline performance:**
    - My serial, unoptimized code that is the baseline for performance runs
      `make INFILE=lac1_novl2.xyz OUTFILE=out.xyz NINTERVALS=101` in about 38
      seconds on Deepthought
